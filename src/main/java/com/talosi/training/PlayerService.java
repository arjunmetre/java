package com.talosi.training;

import com.talosi.training.dao.PlayerDao;
import com.talosi.training.dto.PlayerDto;
import com.talosi.training.entity.Player;
import com.talosi.training.exception.NotFoundException;
import com.talosi.training.mapper.PlayerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class PlayerService {
    private PlayerDao playerDao;

    @Autowired
    public PlayerService(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    public List<PlayerDto> findAll() {
        return PlayerMapper.INSTANCE.mapToDto(playerDao.findAll());
    }
    public PlayerDto  findByPk(String id) {
        return PlayerMapper.INSTANCE.map(
                playerDao.findById(id).orElseThrow(() -> new NotFoundException("player " + id + " not found"))
        );
    }

    @Transactional
    public PlayerDto create(PlayerDto player) {
        UUID uuid = UUID.randomUUID();
        player.setId(uuid.toString());

        return PlayerMapper.INSTANCE.map(
            playerDao.save(PlayerMapper.INSTANCE.map(player))
        );
    }

    public PlayerDto update(PlayerDto player) {
        this.findByPk(player.getId()); // throw NotFoundException if user doesn't exist
        return PlayerMapper.INSTANCE.map(
                playerDao.save(PlayerMapper.INSTANCE.map(player))
        );
    }

    public void delete(String id) {
        PlayerDto player = this.findByPk(id); // throw NotFoundException if user doesn't exist
        playerDao.delete(PlayerMapper.INSTANCE.map(player));
    }
}
