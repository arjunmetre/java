package com.talosi.training;

import com.talosi.training.dao.PlayerDao;
import com.talosi.training.dto.FightDto;
import com.talosi.training.dto.PlayerDto;
import com.talosi.training.dto.TeamDto;
import com.talosi.training.exception.NotFoundException;
import com.talosi.training.mapper.PlayerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FightService {
    private PlayerDao playerDao;

    public FightService(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    public FightDto fight(FightDto fight) {
        List<TeamDto> teams = this.fillPlayersOnTeam(fight);

        teams.forEach( t -> {
            t.setPower(t.getPlayers().stream()
                    .map(p-> Math.random() * p.getPower())
                    .reduce(0d, (a,b) -> a + b)
            );
        });

        teams.sort( (t1, t2) -> (int) (t2.getPower() - t1.getPower()));

        return FightDto.builder()
                .teams(teams)
                .winner(teams.get(0).getName())
                .build();
    }

    private List<TeamDto> fillPlayersOnTeam(FightDto fight) {
        return fight.getTeams().stream().map( (t) ->
            t.toBuilder()
                .players(PlayerMapper.INSTANCE.mapToDto(
                        t.getPlayers().stream()
                            .map( p -> playerDao.findUserByUsername(p.getUsername()))
                            .collect(Collectors.toList())
                ))
                .build()
        ).collect(Collectors.toList());
    }
}
