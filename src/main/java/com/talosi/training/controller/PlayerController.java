package com.talosi.training.controller;

import com.talosi.training.PlayerService;
import com.talosi.training.dto.PlayerDto;
import com.talosi.training.dto.health.HealthDto;
import com.talosi.training.dto.health.HealthServeurDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @ApiOperation(value = "Find all players")
    @GetMapping
    public List<PlayerDto> findAll() {
        return playerService.findAll();
    }

    @ApiOperation(value = "Find one player")
    @GetMapping("/{id}")
    public PlayerDto findOne(
            @ApiParam(value = "player id", required = true)
            @PathVariable("id") String id
    ) {
        return playerService.findByPk(id);
    }

    @ApiOperation(value = "create one player")
    @PutMapping()
    public PlayerDto create(@RequestBody PlayerDto player) {
        return playerService.create(player);
    }

    @ApiOperation(value = "delete one player")
    @DeleteMapping("{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        playerService.delete(id);
        return ResponseEntity.ok("ok");
    }


}
