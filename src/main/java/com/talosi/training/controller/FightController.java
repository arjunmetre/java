package com.talosi.training.controller;

import com.talosi.training.FightService;
import com.talosi.training.PlayerService;
import com.talosi.training.dto.FightDto;
import com.talosi.training.dto.PlayerDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/figths")
public class FightController {

    @Autowired
    private FightService fightService;

    @ApiOperation(value = "organize a fight between players. Only username are necessary on Player object")
    @PostMapping
    public FightDto fight(@RequestBody FightDto fight) {
        return fightService.fight(fight);
    }

}
