package com.talosi.training.mapper;

import com.talosi.training.dto.PlayerDto;
import com.talosi.training.entity.Player;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PlayerMapper {
    PlayerMapper INSTANCE = Mappers.getMapper(PlayerMapper.class);

    PlayerDto map(Player language);
    Player map(PlayerDto language);

    List<PlayerDto> mapToDto(List<Player> language);
    List<Player> mapToEntity(List<PlayerDto> language);

}
