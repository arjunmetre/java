package com.talosi.training.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;


public enum SexEnum implements Serializable {
    FEMALE, MALE
}
