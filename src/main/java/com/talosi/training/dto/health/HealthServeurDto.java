package com.talosi.training.dto.health;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HealthServeurDto {
    String status;
}
