package com.talosi.training.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class TeamDto implements Serializable {

    @ApiModelProperty(value = "Team name")
    private String name;

    @ApiModelProperty(value = "Team players")
    private List<PlayerDto> players;

    @ApiModelProperty(value = "Final power of the team, calculate by the application")
    private double power;
}
