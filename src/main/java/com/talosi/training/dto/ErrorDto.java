package com.talosi.training.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto implements Serializable {
    @ApiModelProperty(value = "code status of the error")
    private String status;
    @ApiModelProperty(value = "message of the error")
    private String error;
}
