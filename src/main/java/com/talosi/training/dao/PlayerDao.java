package com.talosi.training.dao;

import com.talosi.training.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerDao extends JpaRepository<Player, String> {

    @Query("from Player where username=:username")
    public Player findUserByUsername(String username);
}
